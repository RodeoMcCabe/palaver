tool

extends WindowDialog

var controller # hold reference to palaver.gd
var active_item # keep a reference to whatever we're working on at the moment
var file_name = ""

onready var close_button = get_close_button()
onready var tree = $"VBox/HSplit/Tree"

onready var file_menu = $"VBox/HBox/FileMenu".get_popup()
onready var conv_menu = $"VBox/HBox/ConvMenu".get_popup()

# CONVERSATION PANEL CONTROLS
onready var conv_id_lbl = $"VBox/HSplit/VBox/ConvPanel/VBox/Grid/IdCont/IdTextLbl"
onready var conv_id_edit = $"VBox/HSplit/VBox/ConvPanel/VBox/Grid/IdCont/IdLineEdit"
onready var conv_id_edit_btn = $"VBox/HSplit/VBox/ConvPanel/VBox/Grid/IdEditBtn"

onready var conv_desc_lbl = $"VBox/HSplit/VBox/ConvPanel/VBox/Grid/DescCont/DescTextLbl"
onready var conv_desc_edit = $"VBox/HSplit/VBox/ConvPanel/VBox/Grid/DescCont/DescLineEdit"
onready var conv_desc_edit_btn = $"VBox/HSplit/VBox/ConvPanel/VBox/Grid/DescEditBtn"

onready var conv_initial_rmk_opt = $"VBox/HSplit/VBox/ConvPanel/VBox/Grid/InitialRmkOpt"

# control that acts as a container for the remark and responses panels
onready var lower_cont = $"VBox/HSplit/VBox/LowerCont"

# REMARK PANEL CONTROLS
onready var rmk_panel = $"VBox/HSplit/VBox/LowerCont/RmkPanel"

onready var rmk_id_lbl = $"VBox/HSplit/VBox/LowerCont/RmkPanel/VBox/Grid/IdCont/IdTextLbl"
onready var rmk_id_edit = $"VBox/HSplit/VBox/LowerCont/RmkPanel/VBox/Grid/IdCont/IdLineEdit"
onready var rmk_id_edit_btn = $"VBox/HSplit/VBox/LowerCont/RmkPanel/VBox/Grid/IdEditBtn"

onready var rmk_content_lbl = $VBox/HSplit/VBox/LowerCont/RmkPanel/VBox/Grid/ContentCont/ContentLbl
onready var rmk_content_edit = $VBox/HSplit/VBox/LowerCont/RmkPanel/VBox/Grid/ContentCont/ContentTextEdit
onready var rmk_content_edit_btn = $"VBox/HSplit/VBox/LowerCont/RmkPanel/VBox/Grid/ContentEditBtn"

onready var choices_list = $VBox/HSplit/VBox/LowerCont/RmkPanel/VBox/Grid/Panel/VBox/HBox2/ChoicesList
onready var choice_flags_list = $VBox/HSplit/VBox/LowerCont/RmkPanel/VBox/Grid/Panel/VBox/HBox2/FlagsList
onready var choice_link_lbl = $VBox/HSplit/VBox/LowerCont/RmkPanel/VBox/Grid/Panel/VBox/HBox/LinkTextLbl
onready var choices_edit_btn = $VBox/HSplit/VBox/LowerCont/RmkPanel/VBox/Grid/ChoicesEditBtn


# RESPONSES PANEL CONTROLS
onready var rsp_panel = $"VBox/HSplit/VBox/LowerCont/RspPanel"
onready var rsp_id_lbl = $"VBox/HSplit/VBox/LowerCont/RspPanel/VBox/Grid/IdCont/IdTextLbl"
onready var rsp_id_edit = $"VBox/HSplit/VBox/LowerCont/RspPanel/VBox/Grid/IdCont/IdLineEdit"
onready var rsp_id_edit_btn = $"VBox/HSplit/VBox/LowerCont/RspPanel/VBox/Grid/IdEditBtn"

onready var rsp_content_lbl = $"VBox/HSplit/VBox/LowerCont/RspPanel/VBox/Grid/ContentCont/ContentTextLbl"
onready var rsp_content_edit = $"VBox/HSplit/VBox/LowerCont/RspPanel/VBox/Grid/ContentCont/ContentLineEdit"
onready var rsp_content_edit_btn = $"VBox/HSplit/VBox/LowerCont/RspPanel/VBox/Grid/ContentEditBtn"

onready var rsp_links_list = $VBox/HSplit/VBox/LowerCont/RspPanel/VBox/Grid/LinksList


func _ready():
	close_button.connect("pressed", controller, "close_designer_view")
	init_file_menu()
	init_conv_menu()
	tree.select_mode = Tree.SELECT_ROW
	print("designer view ready")


func init_file_menu():
	file_menu.clear()
	file_menu.add_item("New", 0, KEY_MASK_CTRL | KEY_N) # sets hotkey to ctrl+N
	file_menu.add_item("Save", 1, KEY_MASK_CTRL | KEY_S)
	file_menu.add_item("Open", 2, KEY_MASK_CTRL | KEY_O)
	file_menu.connect("index_pressed", self, "_on_FileMenu_pressed")


func init_conv_menu():
	conv_menu.clear()
	conv_menu.add_item("Add Conversation", 0, KEY_MASK_CTRL | KEY_MASK_SHIFT | KEY_N)
	conv_menu.add_item("Add NPC Remark", 1, KEY_MASK_CTRL | KEY_K)
	conv_menu.add_item("Add Response", 2, KEY_MASK_CTRL | KEY_R)
	conv_menu.connect("index_pressed", self, "_on_ConvMenu_pressed")


func enable_editing(is_enabled):
	if is_enabled:
		for i in range(0, conv_menu.get_item_count()):
			conv_menu.set_item_disabled(i, false)

		conv_id_edit_btn.disabled = false
		conv_desc_edit_btn.disabled = false
		conv_initial_rmk_opt.disabled = false
		conv_id_edit_btn.disabled = false

		rmk_id_edit_btn.disabled = false
		rmk_content_edit_btn.disabled = false
		choices_edit_btn.disabled = false

		rsp_id_edit_btn.disabled = false
		rsp_content_edit_btn.disabled = false
	else:
		for i in range(0, conv_menu.get_item_count()):
			conv_menu.set_item_disabled(i, true)

		conv_id_edit_btn.disabled = true
		conv_desc_edit_btn.disabled = true
		conv_initial_rmk_opt.disabled = true
		conv_id_edit_btn.disabled = true

		rmk_id_edit_btn.disabled = true
		rmk_content_edit_btn.disabled = true
		choices_edit_btn.disabled = true

		rsp_id_edit_btn.disabled = true
		rsp_content_edit_btn.disabled = true


# Updates the whole view to reflect current data.
func update_view(data):
	update_tree(data)
	update()


# Fills the tree view with the contents of the passed dictionary.
# file_name will be the root node of the tree, under which all conversations
# are listed.
# To Do: Break this into smaller functions: maybe tree_set_file_name(),
# tree_load_conversations(), tree_load_remarks() ....
func update_tree(data):
	tree.clear()

	var tree_root = tree.get_root()
	if tree_root == null:
		tree_root = tree.create_item()
	tree_root.set_text(0, file_name)

	# put all the conversations in the tree
	for conv in data.keys():
		var conv_tree_item = tree.create_item(tree_root)
		conv_tree_item.set_text(0, conv)

		# make some TreeItems for organization
		var remarks = tree.create_item(conv_tree_item)
		remarks.set_text(0, "NPC Remarks")
		var responses = tree.create_item(conv_tree_item)
		responses.set_text(0, "Responses")


		for rmk in data[conv]["remarks"].keys():
			var rmk_tree_item = tree.create_item(remarks)
			rmk_tree_item.set_text(0, rmk)
		for rsp in data[conv]["responses"].keys():
			var rsp_tree_item = tree.create_item(responses)
			rsp_tree_item.set_text(0, rsp)


func update_conv_panel(conv_id):
	conv_id_lbl.text = conv_id
	conv_desc_lbl.text = controller.get_conv_desc(conv_id)

	conv_initial_rmk_opt.clear()
	var remarks = controller.get_conv_rmks(conv_id)
	var i = 0
	for rmk_id in remarks.keys():
		var s = rmk_id +  " - " + remarks[rmk_id]["content"].substr(0, 25)
		conv_initial_rmk_opt.add_item(s, i)
		conv_initial_rmk_opt.set_item_metadata(i, rmk_id)
		i += 1

	# select the initial remark
	for i in range(0, conv_initial_rmk_opt.get_item_count()):
		if conv_initial_rmk_opt.get_item_metadata(i) == controller.get_conv_initial_rmk(conv_id):
			conv_initial_rmk_opt.select(i)
			break


func update_rmk_panel(id):
	rmk_id_lbl.text = id
	rmk_content_lbl.text = controller.get_rmk_content(conv_id_lbl.text, id)

	choice_flags_list.clear()
	choices_list.clear()
	var choices = controller.get_rmk_choices(conv_id_lbl.text, id)
	var i = 0
	for c in choices.keys():
		choices_list.add_item(c + " - " + choices[c]["response"])
		choices_list.set_item_metadata(i, c)
		i += 1


func update_rsp_panel(id):
	rsp_id_lbl.text = id
	rsp_content_lbl.text = controller.get_rsp_content(conv_id_lbl.text, id)

	rsp_links_list.clear()
	var links = controller.get_rsp_links(conv_id_lbl.text, rsp_id_lbl.text)
	var i = 0
	for rmk_id in links.keys():
		rsp_links_list.add_item(rmk_id + " - " + links[rmk_id].substr(0, 25))
		rsp_links_list.set_item_metadata(0, rmk_id)
		i += 1


# Handles the editing of values that have an edit button next to them.
# Passed parameters are the Control nodes associated with the field.
func edit_field(label, textedit, btn):
	if textedit is LineEdit:
		textedit.editable = true
	elif textedit is TextEdit:
		textedit.readonly = false
	else:
		printerr("ERROR: invalid Control passed to designer-veiw.gd::edit_field()")
	textedit.visible = true
	textedit.text = label.text
	textedit.select_all()
	textedit.grab_focus()

	label.visible = false
	btn.text = "OK"



################################################################################################
# CALLBACKS #
################################################################################################

func _on_WindowDialog_popup_hide():
	controller.close_designer_view()


func _on_FileMenu_pressed(idx):
	print(file_menu.get_item_text(idx) + " pressed")
	if idx == 0: # new file
		controller.new_file()

func _on_ConvMenu_pressed(idx):
	print(conv_menu.get_item_text(idx) + " pressed")
	if idx == 0: # add conversation
		controller.add_conversation()
	elif idx == 1: # add NPC remark
		controller.add_remark(conv_id_lbl.text)
	elif idx == 2: # add response
		controller.add_response(conv_id_lbl.text)
	update_tree(controller.get_conversations())


# Updates the view to display the selected item.
func _on_Tree_item_selected():
	active_item = tree.get_selected()
	var parent = active_item.get_parent()
	if parent != null: # don't do anything with root item
		# find the conversation that the selected item belongs to and update conv panel
		var ancestor = parent
		if ancestor.get_parent() != null:
			while not ancestor.get_parent().get_text(0).ends_with(".json"):
				ancestor = ancestor.get_parent()
			update_conv_panel(ancestor.get_text(0))
		else:
			update_conv_panel(active_item.get_text(0))

		if parent.get_text(0) == "NPC Remarks":
			rmk_panel.visible = true
			rsp_panel.visible = false
			update_rmk_panel(active_item.get_text(0))
			choice_link_lbl.text = ""
		elif parent.get_text(0) == "Responses":
			rsp_panel.visible = true
			rmk_panel.visible = false
			update_rsp_panel(active_item.get_text(0))


func _on_ConvIdEditBtn_pressed():
	if conv_id_lbl.visible:
		edit_field(conv_id_lbl, conv_id_edit, conv_id_edit_btn)
	else:
		_on_ConvIdLineEdit_text_entered(conv_id_edit.text)

func _on_ConvIdLineEdit_text_entered(new_id):
	var id = new_id.strip_edges()
	if id == "":
		controller.alert(controller.ALERT_ERROR, "ID cannot be blank!")
		return
	controller.set_conv_id(conv_id_lbl.text, id)
	conv_id_edit.editable = false
	conv_id_edit.visible = false
	conv_id_lbl.visible = true
	conv_id_edit_btn.text = "Edit"
	update_tree(controller.get_conversations())
	update_conv_panel(id)


func _on_ConvDescEditBtn_pressed():
	if conv_desc_lbl.visible:
		edit_field(conv_desc_lbl, conv_desc_edit, conv_desc_edit_btn)
	else:
		_on_ConvDescLineEdit_text_entered(conv_desc_edit.text)

func _on_ConvDescLineEdit_text_entered(new_text):
	var desc = new_text.strip_edges()
	if desc == "":
		controller.alert(controller.ALERT_WARNING, "Description is blank! Adding a description is recommended.")
	controller.set_conv_desc(conv_id_lbl.text, desc)
	conv_desc_edit.editable = false
	conv_desc_edit.visible = false
	conv_desc_lbl.visible = true
	conv_desc_edit_btn.text = "Edit"
	update_tree(controller.get_conversations())
	update_conv_panel(conv_id_lbl.text)


func _on_RmkIdEditBtn_pressed():
	if rmk_id_lbl.visible:
		edit_field(rmk_id_lbl, rmk_id_edit, rmk_id_edit_btn)
	else:
		_on_RmkIdLineEdit_text_entered(rmk_id_edit.text)

func _on_RmkIdLineEdit_text_entered(new_id):
	var id = new_id.strip_edges()
	if id == "":
		controller.alert(controller.ALERT_ERROR, "ID cannot be blank!")
		return
	controller.set_rmk_id(conv_id_lbl.text, rmk_id_lbl.text, id)
	rmk_id_edit.editable = false
	rmk_id_edit.visible = false
	rmk_id_lbl.visible = true
	rmk_id_edit_btn.text = "Edit"
	update_tree(controller.get_conversations())
	update_conv_panel(conv_id_lbl.text)
	update_rmk_panel(id)


func _on_RmkContentEditBtn_pressed():
	if rmk_content_lbl.visible:
		edit_field(rmk_content_lbl, rmk_content_edit, rmk_content_edit_btn)
	else: # textedit is multiline, so pressing enter just makes a newline
		var content = rmk_content_edit.text.strip_edges()
		if content == "":
			controller.alert(controller.ALERT_ERROR, "Content cannot be blank!")
			return
		controller.set_rmk_content(conv_id_lbl.text,
								   rmk_id_lbl.text,
								   content)
		rmk_content_edit.readonly = true
		rmk_content_edit.visible = false
		rmk_content_lbl.visible = true
		rmk_content_edit_btn.text = "Edit"
		update_tree(controller.get_conversations())
		update_conv_panel(conv_id_lbl.text)
		update_rmk_panel(rmk_id_lbl.text)


func _on_ChoicesList_item_selected(index):
	var choice_id = choices_list.get_item_metadata(index)
	var choices = controller.get_rmk_choices(conv_id_lbl.text, rmk_id_lbl.text)

	choice_link_lbl.text = choices[choice_id]["link"]
	choice_flags_list.clear()

	var i = 0
	for id in choices[choice_id]["flags"].keys():
		choice_flags_list.add_item(id + " - " + str(choices[choice_id]["flags"][id]))
		choice_flags_list.set_item_metadata(i, id)
		i += 1


func _on_ChoicesEditBtn_pressed():
	print("ChoicesEditBtn pressed")
	controller.open_edit_choices_view(conv_id_lbl.text, rmk_id_lbl.text)


func _on_RspIdEditBtn_pressed():
	if rsp_id_lbl.visible:
		edit_field(rsp_id_lbl, rsp_id_edit, rsp_id_edit_btn)
	else:
		_on_RspIdLineEdit_text_entered(rsp_id_edit.text)

func _on_RspIdLineEdit_text_entered(new_id):
	var id = new_id.strip_edges()
	if id == "":
		controller.alert(controller.ALERT_ERROR, "ID cannot be blank!")
		return
	controller.set_rsp_id(conv_id_lbl.text, rsp_id_lbl.text, id)
	rsp_id_edit.editable = false
	rsp_id_edit.visible = false
	rsp_id_lbl.visible = true
	rsp_id_edit_btn.text = "Edit"
	update_tree(controller.get_conversations())
	update_rsp_panel(id)


func _on_RspContentEditBtn_pressed():
	if rsp_content_lbl.visible:
		edit_field(rsp_content_lbl, rsp_content_edit, rsp_content_edit_btn)
	else:
		var content = rsp_content_edit.text.strip_edges()
		if content == "":
			controller.alert(controller.ALERT_ERROR, "Content cannot be blank!")
			return
		controller.set_rsp_content(conv_id_lbl.text,
								   rsp_id_lbl.text,
								   content)
		rsp_content_edit.readonly = true
		rsp_content_edit.visible = false
		rsp_content_lbl.visible = true
		rsp_content_edit_btn.text = "Edit"
		update_tree(controller.get_conversations())
		update_rsp_panel(rsp_id_lbl.text)


func _on_InitialRmkOpt_item_selected(id):
	controller.set_conv_initial_rmk(conv_id_lbl.text, conv_initial_rmk_opt.get_item_metadata(id))
