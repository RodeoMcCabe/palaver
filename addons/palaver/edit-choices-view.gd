tool

extends AcceptDialog

onready var all_rsps_list = $HBox/Grid/AllRspList
onready var added_rsps_list = $HBox/Grid/AddedRspList

onready var rsp_add_btn = $HBox/Grid/VBox/RspAddBtn
onready var rsp_remove_btn = $HBox/Grid/VBox/RspRemoveBtn

onready var choice_id_lbl = $HBox/Grid/HBox/Container/ChoiceIdLbl
onready var choice_id_edit = $HBox/Grid/HBox/Container/ChoiceIdLineEdit
onready var choice_id_edit_btn = $HBox/Grid/HBox/ChoiceIdEditBtn
onready var choice_link_opt = $HBox/Grid/LinkOpt

onready var flag_key_edit = $HBox/VBox/HBox/KeyLineEdit
onready var flag_value_edit = $HBox/VBox/HBox/ValueLineEdit
onready var flag_add_btn = $HBox/VBox/FlagsAddBtn
onready var flag_remove_btn = $HBox/VBox/FlagsRemoveBtn
onready var flags_list = $HBox/VBox/FlagsList

var controller
var conv_id = ""
var rmk_id = ""


func _ready():
	update_rsp_lists()


func update_rsp_lists():
	all_rsps_list.clear()
	added_rsps_list.clear()
	
	var responses = controller.get_conv_rsps(conv_id)
	var choices = controller.get_rmk_choices(conv_id, rmk_id)
	var choice_rsps = []
	var i = 0
	
	for choice_id in choices.keys():
		var rsp = choices[choice_id]["response"]
		added_rsps_list.add_item(choice_id + " - " + rsp)
		added_rsps_list.set_item_metadata(i, choice_id)
		choice_rsps.append(choices[choice_id]["response"])
		i += 1
	
	i = 0
	for rsp_id in responses.keys():
		# don't add it to all_rsps_list if it's already in added_rsps_list
		if not choice_rsps.has(rsp_id):
			all_rsps_list.add_item(rsp_id + " - " + responses[rsp_id]["content"].substr(0, 25))
			all_rsps_list.set_item_metadata(i, rsp_id)
			i += 1


func update_choice_link_opt():
	choice_link_opt.clear()
	var possible_links = controller.get_conv_rmks(conv_id)
	
	var i = 0
	for link in possible_links.keys():
		if link != rmk_id: # can't link to the remark this choice belongs to
			choice_link_opt.add_item(link + " - " + possible_links[link]["content"].substr(0, 25))
			choice_link_opt.set_item_metadata(i, link)
			i += 1


func update_flags_list():
	flags_list.clear()
	var selected = added_rsps_list.get_selected_items()
	if selected.size() > 0:
		var flags_dict = controller.get_choice_flags(conv_id, rmk_id, selected[0])
		for flag_id in flags_dict.keys():
			flags_list.add_item(flag_id + " - " + str(flags_dict[flag_id]))
			flags_list.set_item_metadata(flags_list.get_item_count() - 1, flag_id)


func _on_ConfirmDialog_confirmed():
	pass # write to data model


func _on_ConfirmDialog_popup_hide():
	controller.close_edit_choices_view() # discard changes


func _on_AddedRspList_item_selected(index):
	var id = added_rsps_list.get_item_metadata(index)
	var choice = controller.get_rmk_choices(conv_id, rmk_id)[id]
	choice_id_lbl.text = id
	choice_id_edit_btn.disabled = false
	update_choice_link_opt()
	update_flags_list()


func _on_ChoiceIdEditBtn_pressed():
	if choice_id_lbl.visible:
		choice_id_lbl.visible = false
		choice_id_edit.visible = true
		choice_id_edit.editable = true
		choice_id_edit.text = choice_id_lbl.text
		choice_id_edit.select_all()
		choice_id_edit.grab_focus()
		choice_id_edit_btn.text = "OK"
	else:
		_on_ChoiceIdLineEdit_text_entered(choice_id_edit.text)

func _on_ChoiceIdLineEdit_text_entered(new_text):
	var choice_dict = controller.get_rmk_choices(conv_id, rmk_id)
	var new_id = controller.get_unique_id(new_text, choice_dict)

	controller.set_choice_id(conv_id, rmk_id, choice_id_lbl.text, new_id)
	choice_id_lbl.text = new_id
	choice_id_lbl.visible = true
	choice_id_edit.visible = false
	choice_id_edit.editable = false

	var item_index = added_rsps_list.get_selected_items()[0]
	added_rsps_list.set_item_text(item_index, new_id + " - " + choice_dict[new_id]["response"])
	added_rsps_list.set_item_metadata(item_index, new_id)
	choice_id_edit_btn.text = "Edit"


func _on_LinkOpt_item_selected(index):
	var link_id = choice_link_opt.get_item_metadata(index)
	var choice_id = added_rsps_list.get_item_metadata(added_rsps_list.get_selected_items()[0])
	controller.set_choice_link(conv_id, rmk_id, choice_id, link_id)


func _on_RspAddBtn_pressed():
	var selected = all_rsps_list.get_selected_items()
	if selected.size() > 0:
		var rsp_id = all_rsps_list.get_item_metadata(selected[0])
		controller.add_rmk_choice(conv_id, rmk_id, rsp_id)
		update_rsp_lists()


func _on_RspRemoveBtn_pressed():
	var selected = added_rsps_list.get_selected_items()
	if selected.size() > 0:
		var choices_dict = controller.get_rmk_choices(conv_id, rmk_id)
		choices_dict.erase(added_rsps_list.get_item_metadata(selected[0]))
		update_rsp_lists()
