# The main script for the designer. Acts as a controller between palaver-data.gd
# and the view scenes.

#tool
#extends EditorPlugin
extends Node


enum {
	ALERT_INFO,
	ALERT_WARNING,
	ALERT_ERROR }


const AlertPopup = preload("res://addons/palaver/AlertPopup.tscn")


#var editor_tree
var tree_root

onready var data = preload("res://addons/palaver/palaver-data.gd").new()

var designer_view
var edit_choices_view


func _ready():
	open_designer_view()


func _enter_tree():
	pass


	# initialize the button in the editor which opens the designer GUI
#	editor_tree = Tree.new()

#	tree_root = editor_tree.create_item()
#	tree_root.set_text(0, "Palaver")
#	editor_tree.connect("item_activated", self, "open_designer_view")
#	add_control_to_dock(DOCK_SLOT_LEFT_UR, editor_tree)
#	add_control_to_container(CONTAINER_TOOLBAR, editor_tree)

	# initalize the GUI


func _exit_tree():
#	remove_control_from_docks(editor_tree)
	pass


func open_designer_view():
	designer_view = load("res://addons/palaver/DesignerView.tscn").instance()
	designer_view.controller = self
	add_child(designer_view)
	designer_view.popup_centered()


func close_designer_view():
	remove_child(designer_view)
	designer_view.queue_free()


func open_edit_choices_view(conv_id, rmk_id):
	edit_choices_view = load("res://addons/palaver/EditChoicesView.tscn").instance()
	edit_choices_view.controller = self
	edit_choices_view.conv_id = conv_id
	edit_choices_view.rmk_id = rmk_id
	add_child(edit_choices_view)
	edit_choices_view.popup_centered()


func close_edit_choices_view():
	remove_child(edit_choices_view)
	edit_choices_view.queue_free()
	designer_view.update_view(data.conversations)


func new_file():
	if data.file == null:
		data.new_file()

	designer_view.file_name = data.file_name
	designer_view.enable_editing(true)
	designer_view.update_view(data.conversations)
	var next = designer_view.tree.get_root().get_children()
	designer_view.update_conv_panel(next.get_text(0))


func alert(alert_type, message):
	var dlg = AlertPopup.instance()
	add_child(dlg)
	dlg.controller = self
	dlg.set_alert_type(alert_type)
	dlg.set_message(message)
	dlg.popup_centered()
	
	yield(dlg, "popup_hide")


func get_file_name():
	return data.file_name


func get_conversations():
	return data.conversations


func get_conv_desc(id):
	return data.conversations[id]["description"]


func get_conv_rmks(conv_id):
	return data.conversations[conv_id]["remarks"]


func get_conv_rsps(conv_id):
	return data.conversations[conv_id]["responses"]


func get_conv_initial_rmk(conv_id):
	return data.conversations[conv_id]["initial_rmk"]


func get_rmk_content(conv_id, rmk_id):
	return data.conversations[conv_id]["remarks"][rmk_id]["content"]


func get_rmk_choices(conv_id, rmk_id):
	return data.conversations[conv_id]["remarks"][rmk_id]["choices"]


func get_choice_flags(conv_id, rmk_id, choice_id):
	return data.conversations[conv_id]["remarks"][rmk_id]["choices"][choice_id]["flags"]


func get_rsp_content(conv_id, rsp_id):
	return data.conversations[conv_id]["responses"][rsp_id]["content"]


func get_rsp_links(conv_id, rsp_id):
	var links = {}
	# holy fuckin dictionaries batman
	for rmk_id in data.conversations[conv_id]["remarks"].keys():
		for choice_id in data.conversations[conv_id]["remarks"][rmk_id]["choices"].keys():
			if data.conversations[conv_id]["remarks"][rmk_id]["choices"][choice_id]["response"] == rsp_id:
				links[rmk_id] = data.conversations[conv_id]["remarks"][rmk_id]["content"]
	return links


# TO DO: check file extension
func set_file_name(new_name):
	if new_name != "":
		data.file_name = new_name
	else:
		alert(ALERT_ERROR, "File name cannot be blank!")
		return
	print("File name set: " + new_name)


func set_conv_id(old_id, new_id):
	if data.conversations.has(new_id):
		printerr("WARNING: there is already a conversation with this ID! Overwriting ...")
	data.conversations[new_id] = data.conversations[old_id]
	data.conversations.erase(old_id)
	print("Conversation ID set: " + new_id)


# Must pass the id of the conversation being edited.
func set_conv_desc(conv_id, desc):
	data.conversations[conv_id]["description"] = desc
	print("Conversation description set: " + desc)


func set_conv_initial_rmk(conv_id, rmk_id):
	data.conversations[conv_id]["initial_rmk"] = rmk_id
	print("Conversation initial remark set: " + rmk_id)


func set_rmk_id(conv_id, old_id, new_id):
	if data.conversations[conv_id]["remarks"].has(new_id):
		printerr("WARNING: there is already a remark with this ID! Overwriting ...")
	data.conversations[conv_id]["remarks"][new_id] = data.conversations[conv_id]["remarks"][old_id]
	data.conversations[conv_id]["remarks"].erase(old_id)
	print("Remark ID set: " + new_id)


func set_rmk_content(conv_id, rmk_id, new_text):
	data.conversations[conv_id]["remarks"][rmk_id]["content"] = new_text
	print("Remark content set: " + new_text)


func set_rsp_id(conv_id, old_id, new_id):
	if data.conversations[conv_id].has(new_id):
		printerr("WARNING: there is already a response with this ID! Overwriting ...")
	data.conversations[conv_id]["responses"][new_id] = data.conversations[conv_id]["responses"][old_id]
	data.conversations[conv_id]["responses"].erase(old_id)
	print("Response ID set: " + new_id)


func set_rsp_content(conv_id, rsp_id, new_text):
	data.conversations[conv_id]["responses"][rsp_id]["content"] = new_text
	print("Response content set: " + new_text)


func set_choice_id(conv_id, rmk_id, old_id, new_id):
	var choices = data.conversations[conv_id]["remarks"][rmk_id]["choices"]
	if choices.has(new_id):
		printerr("WARNING: there is already a choice with this ID! Overwriting ...")
	choices[new_id] = choices[old_id]
	data.conversations[conv_id]["remarks"][rmk_id]["choices"].erase(old_id)
	print("Choice ID set: " + new_id)


func set_choice_link(conv_id, rmk_id, choice_id, link_id):
	data.conversations[conv_id]["remarks"][rmk_id]["choices"][choice_id]["link"] = link_id


func add_conversation():
	if data.file != null:
		var id = get_unique_id("new_conv", data.conversations)
		data.conversations[id] = data.generate_empty_conv()


func add_remark(conv_id):
	if data.file != null:
		var id = get_unique_id("new_rmk", data.conversations[conv_id]["remarks"])
		data.conversations[conv_id]["remarks"][id] = data.generate_empty_rmk()


func add_rmk_choice(conv_id, rmk_id, rsp_id):
	var choices_dict = data.conversations[conv_id]["remarks"][rmk_id]["choices"]
	var id = get_unique_id("new_choice", choices_dict)
	choices_dict[id] = data.generate_empty_choice()
	choices_dict[id]["response"] = rsp_id
	choices_dict[id]["link"] = get_conv_rmks(conv_id).keys()[0]


func add_response(conv_id):
	if data.file != null:
		var id = get_unique_id("new_rsp", data.conversations[conv_id]["responses"])
		data.conversations[conv_id]["responses"][id] = data.generate_empty_rsp()


func get_unique_id(base_id, dict):
	if dict.has(base_id):
		var i = 2
		while dict.has(base_id + '.' + str(i)):
			i += 1
		return base_id + '.' + str(i)
	else:
		return base_id
