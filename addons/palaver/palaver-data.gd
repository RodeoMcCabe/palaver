tool


var file
var file_name = ""
var conversations = {}


func _exit_tree():
	if file != null:
		file.close()


func load_file(path):
	pass

func save_file():
	pass


func new_file():
	file = File.new()
	file.open("res://new.json", File.WRITE)
	file_name = "new.json"
	init_data()


func generate_empty_conv():
	return { "description" : "[empty]",
			 "remarks" : { "[empty]" : generate_empty_rmk() },
			 "initial_rmk" : "[empty]",
			 "responses" : { "[empty]" : generate_empty_rsp() } 
		   }


func generate_empty_rmk():
	return { "content" : "[empty]",
		     "choices" : generate_empty_choice()
		   }


func generate_empty_choice():
	return { "response" : "[empty]",
			 "link" : "[empty]",
			 "flags" : generate_empty_flag()
		   }


func generate_empty_flag():
	return { "[empty]" : "[empty]" }


func generate_empty_rsp():
	return { "content" : "[empty]" }


func init_data(): # sets all dictionaries to placeholder values
	conversations.clear()

	var responses = {}
	var remarks = {}
	var choices = {}
	var choice_flags = {}
	
	responses["rsp1"] = {"content" : "Default response content."}

	choice_flags["flag1"] = 101

	choices["choice1"] = {
			"response" : "rsp1",
			"link" : "rmk2",
			"flags" : choice_flags
			}

	remarks["rmk1"] = {
			"content" : "Default remark content 1.",
			"choices" : choices
			}
	remarks["rmk2"] = {
			"content" : "Default remark content 2.",
			"choices" : {}
			}

	conversations["conv1"] = {
			"description" : "Default conversation description.",
			"remarks" : remarks,
			"initial_rmk" : "rmk1",
			"responses" : responses
			}