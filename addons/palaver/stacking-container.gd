# Custom container type in which all children take up the whole container,
# stacked one on top of the other.
# Children must have their visibility toggled manually in code for this
# container to be useful.

extends Container

func _ready():
	connect("resized", self, "_on_resized") # so you don't have to do it in the editor every time.

# resizes all children to match the size and position of the container
func _on_resized():
	if get_child_count() > 0:
		for child in get_children():
			fit_child_in_rect(child, Rect2(Vector2(0, 0), rect_size))