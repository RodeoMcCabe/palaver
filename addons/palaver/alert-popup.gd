
extends WindowDialog


const ICON_INFO = preload("res://addons/palaver/info.png")
const ICON_WARNING = preload("res://addons/palaver/warning.png")
const ICON_ERROR = preload("res://addons/palaver/error.png")

onready var icon = $VBox/HBox/Icon
onready var label = $VBox/HBox/Label

var controller

signal hidden

func _unhandled_input(event):
	if visible and popup_exclusive:
		get_tree().set_input_as_handled()


func set_message(message):
	label.text = message


func set_alert_type(type):
	match type:
		controller.ALERT_INFO:
			icon.texture = ICON_INFO
			window_title = "Information"
		controller.ALERT_WARNING:
			icon.texture = ICON_WARNING
			window_title = "Warning!"
		controller.ALERT_ERROR:
			icon.texture = ICON_ERROR
			window_title = "Error!"


func _on_OkBtn_pressed():
	emit_signal("hidden")
	hide()
	queue_free()
